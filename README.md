# Automated Backup #

Copies a single source file to multiple destination directories, optionally keeping multiple revisions of the file.

# Usage #

Application configuration is handled off the app.config.  It's split into three sections.

## Options ##

These are general application configurations.

* shutdown-after-successful-copy: "true" or "false". If the computer should shut down after all files have been successfully backed up.

* verbose-logging: "true" or "false". Additional console logging

* verify-file-integrity: "true" or "false".  If set to true, after files are copied, a hash will be taken of the source and destination to ensure they're the same file.

## Sources ##

The files or directories that will be backed up.  If a directory is specified, its contents (and all subfolders) will be added to a ZIP file before being copied.

* path: The file or directory that is to be copied.  Contents of a directory will be added to a ZIP file and that will be copied.

## Targets ##

The destination where the source files will be copied to.

* path: The destination where each of the source files will be copied to

* revisions: The number of files that should be kept at the destination.  If there are 3 source files and revisions is set to 4, there will be a maximum of 12 files in the target location.

* backup-group: Any string will do.  At least one of the target locations that share the same backup-group value must exist for the copy operation to start.  This is useful if you use multiple external hard drives, but one or more may not be plugged in when starting a backup.

# License #

Copyright 2017 Keith Ott

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.