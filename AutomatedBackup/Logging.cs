﻿using AutomatedBackup.Configuration;
using System;

namespace AutomatedBackup
{
    public static class Logging
    {
        public static bool VerboseLogging { get; } = AutomatedBackupConfigurationSection.GetBackupConfiguration("options").VerboseLogging;

        public static void Log(string message)
        {
            if(VerboseLogging)
            {
                Console.WriteLine(message);
            }
        }

        public static void LogImportant(string message)
        {
            Console.WriteLine(message);
        }
    }
}
