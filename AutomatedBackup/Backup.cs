﻿using AutomatedBackup.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.IO.Compression;
using System.Diagnostics;
using System.Configuration;
using System.Security.Cryptography;

namespace AutomatedBackup
{
    public class Backup
    {
        private AutomatedBackupConfigurationSection _configuration;
        private IList<BackupTargetElement> _targets;
        private IList<BackupSourceElement> _sources;
        private string RevisionTimestampFormat = "yyyyMMddHmmss";
        private string BackupTimestamp;

        public Backup(AutomatedBackupConfigurationSection configuration, IList<BackupTargetElement> targets, IList<BackupSourceElement> sources)
        {
            if (targets == null || !targets.Any()) throw new ArgumentNullException(nameof(targets));
            if (sources == null || !sources.Any()) throw new ArgumentNullException(nameof(sources));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _targets = targets;
            _sources = sources;

            BackupTimestamp = DateTime.Now.ToString(RevisionTimestampFormat);
        }

        public void PerformBackup()
        {
            PreValidation();

            Logging.LogImportant("Performing backup...");

            foreach (var source in _sources)
            {
                string sourceFileName = DetermineSourceFile(source);

                foreach (var target in _targets.Where(target => Directory.Exists(target.Path)))
                {
                    RemoveOldRevisions(sourceFileName, target);

                    var targetFileName = Path.Combine(target.Path, BuildDestinationFileName(sourceFileName));
                    Copy(sourceFileName, targetFileName);
                    if (_configuration.VerifyFileIntegrity)
                        VerifyFileIntegrity(sourceFileName, targetFileName);
                }

                RemoveSourceIfNeeded(source, sourceFileName);
            }

            ShutdownAfterSuccessfulCopy();
            Logging.LogImportant("Backup operation completed successfully!");
        }

        private void ShutdownAfterSuccessfulCopy()
        {
            if (_configuration.ShutdownAfterSuccessfulCopy)
            {
                Logging.LogImportant($"Issuing shutdown command at {DateTime.Now.ToString()}");
                var shutdown = new Process();
                shutdown.StartInfo.FileName = "shutdown";
                shutdown.StartInfo.Arguments = "/s /t 60";
                shutdown.Start();
            }
        }

        private static string DetermineSourceFile(BackupSourceElement source)
        {
            var sourceFile = source.Path;

            if (Directory.Exists(source.Path) && !File.Exists(source.Path))
            {
                sourceFile = Path.Combine(Path.GetTempPath(), $"{Path.GetFileName(Path.GetDirectoryName(source.Path))}.zip");
                if (File.Exists(sourceFile))
                {
                    throw new ApplicationException($"Temp file '{sourceFile}' exists. Unable to proceed.");
                }
                ZipFile.CreateFromDirectory(source.Path, sourceFile);
                Logging.Log($"Zipped contents of {source.Path} to {sourceFile}");
            }

            return sourceFile;
        }

        private static void RemoveSourceIfNeeded(BackupSourceElement source, string sourceFile)
        {
            if (Directory.Exists(source.Path) && !File.Exists(source.Path))
            {
                File.Delete(sourceFile);
                Logging.Log($"Deleted temporary {sourceFile}");
            }
        }

        private void PreValidation()
        {
            Logging.LogImportant("Performing pre-validation...");

            var backupGroups = _targets.GroupBy(target => target.BackupGroup);
            foreach (var group in backupGroups)
            {
                if (_targets.Where(target => target.BackupGroup == group.Key).Count(target => Directory.Exists(target.Path)) == 0)
                {
                    throw new ConfigurationErrorsException($"At least one path must exist in the {group.Key} backup group.");
                }
            }

            var missingSources = _sources.Where(source => !File.Exists(source.Path) && !Directory.Exists(source.Path)).ToList();

            if (missingSources.Any())
            {
                throw new ConfigurationErrorsException(
                    $"The following sources are missing:{Environment.NewLine}" +
                    $" - {string.Join($"{Environment.NewLine} - ", missingSources.Select(source => source.Path).ToList())}");
            }
        }

        private static void RemoveOldRevisions(string source, BackupTargetElement target)
        {
            var files = Directory.GetFiles(target.Path, $"{GetFileName(source)}.*.{GetFileExtension(source)}").ToList();
            var revisionsToSave = files.OrderByDescending(file => file).Take(target.Revisions - 1);
            var revisionsToDelete = files.Where(file => !revisionsToSave.Contains(file));

            foreach (var revision in revisionsToDelete)
            {
                Logging.Log($"Removing old revision '{revision}'");
                File.Delete(revision);
            }
        }

        private static void Copy(string sourceFileName, string targetFileName)
        {
            Logging.Log($"Backing up {sourceFileName} to '{targetFileName}'...");

            using (var reader = new FileStream(sourceFileName, FileMode.Open))
            {
                using (var writer = new FileStream(targetFileName, FileMode.CreateNew))
                {
                    reader.CopyTo(writer);
                }
            }
        }

        private string BuildDestinationFileName(string source)
        {
            return $"{GetFileName(source)}.{BackupTimestamp}.{GetFileExtension(source)}";
        }

        private static string GetFileName(string source)
        {
            return Path.GetFileNameWithoutExtension(source);
        }

        private static string GetFileExtension(string source)
        {
            var extension = Path.GetExtension(source);
            extension = extension.StartsWith(".") ? extension.Substring(1) : extension;
            return extension;
        }

        private static void VerifyFileIntegrity(string source, string target)
        {
            Logging.Log($"Verifying {source} equals {target}...");
            var sourceHash = ComputeHash(source);
            var targetHash = ComputeHash(target);

            if(sourceHash.Length != targetHash.Length || !Enumerable.SequenceEqual(sourceHash, targetHash))
            {
                throw new ApplicationException($"{source} does not equal {target}! File copy failed!");
            }
        }

        private static byte[] ComputeHash(string file)
        {
            using (var stream = new FileStream(file, FileMode.Open))
            {
                using (var sourceSha = SHA256.Create())
                {
                    return sourceSha.ComputeHash(stream);
                }
            }
        }
    }
}
