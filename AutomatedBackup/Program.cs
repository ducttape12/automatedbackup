﻿using AutomatedBackup.Configuration;
using System;
using System.Linq;

namespace AutomatedBackup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // http://linux.byexamples.com/archives/315/how-to-shutdown-and-reboot-without-sudo-password/

            var backup = new Backup(AutomatedBackupConfigurationSection.GetBackupConfiguration("options"),
                BackupTargetConfigurationSection.GetBackupTargets("targets").ToList(),
                BackupSourceConfigurationSection.GetBackupSources("sources").ToList());

            try
            {
                backup.PerformBackup();
            }
            catch(Exception ex)
            {
                Logging.LogImportant($"Exception caught. Error: {ex.ToString()}");
            }

            Console.WriteLine();
            Console.WriteLine("Operation complete. Press enter to exit.");
            Console.Read();
        }
    }
}
