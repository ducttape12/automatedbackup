﻿using System;
using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupSourceElement : ConfigurationElement
    {
        private const string PathElementName = "path";
        
        [ConfigurationProperty(PathElementName, IsKey = true, IsRequired = true)]
        public string Path
        {
            get => base[PathElementName].ToString();
            set => base[PathElementName] = value;
        }
    }
}
