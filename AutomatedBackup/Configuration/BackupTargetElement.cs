﻿using System;
using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupTargetElement : ConfigurationElement
    {
        private const string PathElementName = "path";
        private const string RevisionsElementName = "revisions";
        private const int MinimumNumberOfRevisions = 0;
        private const string BackupGroupElementName = "backup-group";
        
        [ConfigurationProperty(PathElementName, IsKey = true, IsRequired = true)]
        public string Path
        {
            get => base[PathElementName].ToString();
            set => base[PathElementName] = value;
        }

        [ConfigurationProperty(RevisionsElementName, IsRequired = false, DefaultValue = MinimumNumberOfRevisions)]
        public int Revisions
        {
            get
            {
                int.TryParse(base[RevisionsElementName].ToString(), out int revisions);
                return Math.Max(revisions, MinimumNumberOfRevisions);
            }
            set
            {
                if (value < MinimumNumberOfRevisions)
                    throw new ArgumentOutOfRangeException($"{nameof(Revisions)} must be greater than or equal to {MinimumNumberOfRevisions}");
                base[RevisionsElementName] = value;
            }
        }

        [ConfigurationProperty(BackupGroupElementName, DefaultValue = "")]
        public string BackupGroup
        {
            get => base[BackupGroupElementName].ToString();
            set => base[BackupGroupElementName] = value;
        }
    }
}
