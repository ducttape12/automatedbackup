﻿using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupTargetElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new BackupTargetElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((BackupTargetElement)element).Path;
        }
    }
}
