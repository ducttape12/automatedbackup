﻿using System.Collections.Generic;
using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupSourceConfigurationSection : ConfigurationSection
    {
        private const string PropertyName = "";

        public static IEnumerable<BackupSourceElement> GetBackupSources(string section)
        {
            var config = ConfigurationManager.GetSection(section) as BackupSourceConfigurationSection;
            
            foreach(var Source in config.Instances)
            {
                yield return Source as BackupSourceElement;
            }
        }

        [ConfigurationProperty(PropertyName, IsRequired = true, IsDefaultCollection = true)]
        public BackupSourceElementCollection Instances
        {
            get { return (BackupSourceElementCollection)this[PropertyName]; }
            set { this[PropertyName] = value; }
        }
    }
}
