﻿using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupSourceElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new BackupSourceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((BackupSourceElement)element).Path;
        }
    }
}
