﻿using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class AutomatedBackupConfigurationSection : ConfigurationSection
    {
        private const string ShutdownAfterSuccessfulCopyElementName = "shutdown-after-successful-copy";
        private const string VerboseLoggingElementName = "verbose-logging";
        private const string VerifyFileIntegrityElementName = "verify-file-integrity";

        public static AutomatedBackupConfigurationSection GetBackupConfiguration(string section)
        {
            return ConfigurationManager.GetSection(section) as AutomatedBackupConfigurationSection;
        }

        [ConfigurationProperty(ShutdownAfterSuccessfulCopyElementName, IsRequired = false, DefaultValue = false)]
        public bool ShutdownAfterSuccessfulCopy
        {
            get => bool.Parse(base[ShutdownAfterSuccessfulCopyElementName].ToString());
            set => base[ShutdownAfterSuccessfulCopyElementName] = value;
        }

        [ConfigurationProperty(VerboseLoggingElementName, IsRequired = false, DefaultValue = false)]
        public bool VerboseLogging
        {
            get => bool.Parse(base[VerboseLoggingElementName].ToString());
            set => base[VerboseLoggingElementName] = value;
        }

        [ConfigurationProperty(VerifyFileIntegrityElementName, IsRequired = false, DefaultValue = false)]
        public bool VerifyFileIntegrity
        {
            get => bool.Parse(base[VerifyFileIntegrityElementName].ToString());
            set => base[VerifyFileIntegrityElementName] = value;
        }
    }
}
