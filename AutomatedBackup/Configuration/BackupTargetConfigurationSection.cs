﻿using System.Collections.Generic;
using System.Configuration;

namespace AutomatedBackup.Configuration
{
    public class BackupTargetConfigurationSection : ConfigurationSection
    {
        private const string PropertyName = "";

        public static IEnumerable<BackupTargetElement> GetBackupTargets(string section)
        {
            var config = ConfigurationManager.GetSection(section) as BackupTargetConfigurationSection;
            
            foreach(var target in config.Instances)
            {
                yield return target as BackupTargetElement;
            }
        }

        [ConfigurationProperty(PropertyName, IsRequired = true, IsDefaultCollection = true)]
        public BackupTargetElementCollection Instances
        {
            get { return (BackupTargetElementCollection)this[PropertyName]; }
            set { this[PropertyName] = value; }
        }
    }
}
